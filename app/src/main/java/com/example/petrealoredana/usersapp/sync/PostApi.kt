package com.example.petrealoredana.usersapp.sync

import com.example.petrealoredana.usersapp.mvvm.model.Post
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface PostApi {
    /**
     * Get the list of the pots from the API
     */
    @GET("/posts")
    fun getPosts(): Observable<List<Post>>

    @GET("/posts/{id}")
    fun getPostById(@Path("id") id: Int): Observable<Post>
}