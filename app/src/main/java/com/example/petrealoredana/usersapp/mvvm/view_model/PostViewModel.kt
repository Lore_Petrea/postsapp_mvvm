package com.example.petrealoredana.usersapp.mvvm.view_model

import android.arch.lifecycle.MutableLiveData
import com.example.petrealoredana.usersapp.base.BaseViewModel
import com.example.petrealoredana.usersapp.mvvm.model.Post

class PostViewModel : BaseViewModel() {
    private val postTitle = MutableLiveData<String>()
    private val postBody = MutableLiveData<String>()
    val selected = MutableLiveData<Int>()

    fun bind(post: Post) {
        postTitle.value = post.title
        postBody.value = post.body
    }

    fun getPostTitle(): MutableLiveData<String> {
        return postTitle
    }

    fun getPostBody(): MutableLiveData<String> {
        return postBody
    }
}
