package com.example.petrealoredana.usersapp.dagger_injection.component

import com.example.petrealoredana.usersapp.dagger_injection.module.NetworkModule
import com.example.petrealoredana.usersapp.mvvm.view_model.PostItemViewModel
import com.example.petrealoredana.usersapp.mvvm.view_model.PostListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(postListViewModel: PostListViewModel)

    fun inject(postItemViewModel: PostItemViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}