package com.example.petrealoredana.usersapp.base

import android.arch.lifecycle.ViewModel
import com.example.petrealoredana.usersapp.dagger_injection.component.DaggerViewModelInjector
import com.example.petrealoredana.usersapp.dagger_injection.component.ViewModelInjector
import com.example.petrealoredana.usersapp.dagger_injection.module.NetworkModule
import com.example.petrealoredana.usersapp.mvvm.view_model.PostItemViewModel
import com.example.petrealoredana.usersapp.mvvm.view_model.PostListViewModel

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is PostListViewModel -> injector.inject(this)

            is PostItemViewModel -> injector.inject(this)
        }
    }
}