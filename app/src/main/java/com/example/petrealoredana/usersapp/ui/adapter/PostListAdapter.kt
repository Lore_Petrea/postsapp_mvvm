package com.example.petrealoredana.usersapp.ui.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import com.example.petrealoredana.usersapp.R
import com.example.petrealoredana.usersapp.databinding.ItemPostBinding
import com.example.petrealoredana.usersapp.mvvm.model.Post
import com.example.petrealoredana.usersapp.mvvm.view_model.PostListViewModel
import com.example.petrealoredana.usersapp.mvvm.view_model.PostViewModel

class PostListAdapter(val postListViewModel: PostListViewModel) : RecyclerView.Adapter<PostListAdapter.ViewHolder>() {

    lateinit var postList: List<Post>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostListAdapter.ViewHolder {
        val binding: ItemPostBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_post, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PostListAdapter.ViewHolder, position: Int) {
        holder.bind(postList[position], position, postListViewModel)
    }

    override fun getItemCount(): Int {
        return if (::postList.isInitialized) postList.size else 0
    }

    fun updatePostList(postList: List<Post>) {
        this.postList = postList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root) {
        private var viewModel: PostViewModel = PostViewModel()

        fun bind(post: Post, position: Int, postListViewModel: PostListViewModel) {
            viewModel.bind(post)
            binding.setVariable(BR.postListViewModel, postListViewModel)
            binding.setVariable(BR.position, position)
            binding.postViewModel = viewModel
        }

    }
}