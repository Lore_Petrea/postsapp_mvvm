package com.example.petrealoredana.usersapp.mvvm.model

import android.databinding.BaseObservable
import org.parceler.Parcel
import org.parceler.ParcelConstructor

@Parcel(org.parceler.Parcel.Serialization.BEAN)
data class Post @ParcelConstructor constructor(val userId: Int, val id: Int, val title: String, val body: String) : BaseObservable() {
}
