package com.example.petrealoredana.usersapp.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.example.petrealoredana.usersapp.R
import com.example.petrealoredana.usersapp.databinding.ActivityPostListBinding
import com.example.petrealoredana.usersapp.mvvm.model.Post
import com.example.petrealoredana.usersapp.mvvm.view_model.PostListViewModel
import org.parceler.Parcels


class PostListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPostListBinding
    private lateinit var viewModel: PostListViewModel
    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_list)
        binding.aplRvPostList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        viewModel = ViewModelProviders.of(this).get(PostListViewModel::class.java)
        
        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })

        viewModel.selected.observe(this, object : Observer<Post> {
            override fun onChanged(post: Post?) {
                if (post != null)
                    startSinglePostAcctivity(post)
            }

        })
        binding.viewModellll = viewModel

    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }

    private fun startSinglePostAcctivity(post: Post) {
        val intent = Intent(this, SinglePostActivity::class.java)
        val bundle : Bundle = Bundle()
//        bundle.putParcelable("POST", Parcels.wrap(post)); /* Crash line */
        intent.putExtra("POST", Parcels.wrap(post))
        startActivity(intent)
    }
}