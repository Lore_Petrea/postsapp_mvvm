package com.example.petrealoredana.usersapp.mvvm.view_model

import android.arch.lifecycle.MutableLiveData
import com.example.petrealoredana.usersapp.base.BaseViewModel
import com.example.petrealoredana.usersapp.mvvm.model.Post
import com.example.petrealoredana.usersapp.sync.PostApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostItemViewModel() : BaseViewModel() {

    @Inject
    lateinit var postApi: PostApi

    val postTitle = MutableLiveData<String>()

    private lateinit var subscription: Disposable

    fun loadPost(id: Int) {
        subscription = postApi.getPostById(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostStart() }
            .doOnTerminate { onRetrievePostFinish() }
            .subscribe(
                // Add result
                { result -> onRetrievePostSuccess(result) },
                { error -> onRetrievePostError(error) }
            )
    }

    private fun onRetrievePostError(error: Throwable) {
    }

    private fun onRetrievePostSuccess(result: Post) {
        postTitle.value = result.title
    }

    private fun onRetrievePostFinish() {

    }

    private fun onRetrievePostStart() {

    }
}