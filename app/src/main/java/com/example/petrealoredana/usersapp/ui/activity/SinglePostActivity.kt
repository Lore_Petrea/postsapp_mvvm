package com.example.petrealoredana.usersapp.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import com.example.petrealoredana.usersapp.R
import com.example.petrealoredana.usersapp.databinding.ActivitySinglePostBinding
import com.example.petrealoredana.usersapp.mvvm.model.Post
import com.example.petrealoredana.usersapp.mvvm.view_model.PostItemViewModel
import kotlinx.android.synthetic.main.activity_single_post.*
import org.parceler.Parcels

class SinglePostActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySinglePostBinding
    private lateinit var viewModel: PostItemViewModel
    private var id: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_single_post)

        viewModel = ViewModelProviders.of(this).get(PostItemViewModel::class.java)

        id = Parcels.unwrap<Post>(intent.extras.getParcelable<Parcelable>("POST")).id

        viewModel.postTitle.observe(this, Observer { postTtile ->
            if (postTtile != null) asp_tv_title.text = postTtile
        })
        viewModel.loadPost(id)

    }
}